package com.mapswithme.maps.purchase;

import android.support.annotation.NonNull;

public interface AdsRemovalPurchaseCallback
{
  void onValidationFinish(boolean success);
}
