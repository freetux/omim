package com.mapswithme.maps.purchase;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.mapswithme.maps.PrivateVariables;

import java.util.List;

public class PurchaseFactory
{
  private PurchaseFactory()
  {
    // Utility class.
  }

  @NonNull
  public static PurchaseController<AdsRemovalPurchaseCallback> createPurchaseController()
  {
    String yearlyProduct = PrivateVariables.adsRemovalYearlyProductId();
    String monthlyProduct = PrivateVariables.adsRemovalMonthlyProductId();
    String weeklyProduct = PrivateVariables.adsRemovalWeeklyProductId();
    return new AdsRemovalPurchaseController(new PurchaseValidator<AdsRemovalValidationCallback>() {  public void initialize() {}
      public void destroy() {}
      public void validate(@NonNull String purchaseToken) {}
      public boolean hasActivePurchase() { return false; }
      public void addCallback(@NonNull AdsRemovalValidationCallback callback) {}
      public void removeCallback() {} }, new BillingManager<PlayStoreBillingCallback>() { public void initialize(@NonNull Activity context) {};
      public void destroy() {};
      public void launchBillingFlowForProduct(@NonNull String productId) {};
      public boolean isBillingSupported() {  return false; };
      public void queryExistingPurchases() {};
      public void queryProductDetails(@NonNull String... productIds) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {} }, yearlyProduct,
                                            monthlyProduct, weeklyProduct);
  }
}
