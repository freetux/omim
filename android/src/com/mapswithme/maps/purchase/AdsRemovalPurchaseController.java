package com.mapswithme.maps.purchase;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.mapswithme.maps.Framework;
import com.mapswithme.maps.PrivateVariables;
import com.mapswithme.util.ConnectionState;
import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;
import com.mapswithme.util.statistics.Statistics;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class AdsRemovalPurchaseController extends AbstractPurchaseController<AdsRemovalValidationCallback,
    PlayStoreBillingCallback, AdsRemovalPurchaseCallback>
{
  private static final Logger LOGGER = LoggerFactory.INSTANCE.getLogger(LoggerFactory.Type.BILLING);
  private static final String TAG = AdsRemovalPurchaseController.class.getSimpleName();
  @NonNull
  private final AdsRemovalValidationCallback mValidationCallback = new AdValidationCallbackImpl();
  @NonNull
  private final PlayStoreBillingCallback mBillingCallback = new PlayStoreBillingCallbackImpl();
  @NonNull
  private final List<String> mProductIds;

  AdsRemovalPurchaseController(@NonNull PurchaseValidator<AdsRemovalValidationCallback> validator,
                               @NonNull BillingManager<PlayStoreBillingCallback> billingManager,
                               @NonNull String... productIds)
  {
    super(validator, billingManager);
    mProductIds = Collections.unmodifiableList(Arrays.asList(productIds));
  }

  @Override
  void onInitialize(@NonNull Activity activity)
  {
    getValidator().addCallback(mValidationCallback);
    getBillingManager().addCallback(mBillingCallback);
    getBillingManager().queryExistingPurchases();
  }

  @Override
  void onDestroy()
  {
    getValidator().removeCallback();
    getBillingManager().removeCallback(mBillingCallback);
  }

  @Override
  public void queryPurchaseDetails()
  {
    getBillingManager().queryProductDetails(mProductIds);
  }

  private class AdValidationCallbackImpl implements AdsRemovalValidationCallback
  {
    @Override
    public void onValidate(@NonNull AdsRemovalValidationStatus status)
    {
      LOGGER.i(TAG, "Validation status of 'ads removal': " + status);
      if (status == AdsRemovalValidationStatus.VERIFIED)
        Statistics.INSTANCE.trackEvent(Statistics.EventName.INAPP_PURCHASE_VALIDATION_SUCCESS);
      else
        Statistics.INSTANCE.trackPurchaseValidationError(status);
      boolean activateSubscription = status != AdsRemovalValidationStatus.NOT_VERIFIED;
      LOGGER.i(TAG, "Ads removal subscription "
                    + (activateSubscription ? "activated" : "deactivated"));
      Framework.nativeSetActiveRemoveAdsSubscription(activateSubscription);
      if (activateSubscription)
        Statistics.INSTANCE.trackPurchaseProductDelivered(PrivateVariables.adsRemovalVendor());
      if (getUiCallback() != null)
        getUiCallback().onValidationFinish(activateSubscription);
    }
  }

  private class PlayStoreBillingCallbackImpl implements PlayStoreBillingCallback
  {
  }
}
